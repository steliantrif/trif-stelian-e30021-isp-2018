import java.util.Scanner;
import java.util.Random;
public class ex7 {
    public static void main(String[] args) {
            Random random = new Random();
            Scanner input = new Scanner(System.in);
            int attemptnum = 0;
            int maxattempt = 3;
            int num = random.nextInt(99)+1;
            int number;
        do {
                System.out.print("Guess the number: ");
                number = input.nextInt();

                if (number > num)
                    System.out.println("Wrong answer, your number is too high " + number);
                else if (number < num)
                    System.out.println("Wrong answer, your number is too low " + number);
                else
                    System.out.println("Correct! " + number + " was my number! " );
            } while (number != num && ++attemptnum<maxattempt);
            if (attemptnum==maxattempt)
                System.out.println("You lost, the number was"+num);
        }
    }
