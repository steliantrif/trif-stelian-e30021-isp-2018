public class Point {
    double x;
    double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return " the x: " + x + " the y: " + y;
    }



    public static void main(String[] args) {
        Point p1 = new Point(5, 3);
        System.out.println(p1.toString());
        Circle c1=new Circle();
        System.out.println("the circles radius is:"+c1.getRadius()+" and the color is "+c1.color+" and the area is :"+c1.getArea());

        Circle c2=new Circle(3.0,"blue");
        System.out.println("the circles radius is:"+c2.getRadius()+" and the color is: "+c2.color+" and the area is :"+c2.getArea());
        Rectangle r1=new Rectangle();
        System.out.println("The length is"+r1.getLength()+"the width is"+r1.getWidth()+"and the area is:"+r1.getArea());
        Rectangle r2=new Rectangle(3,5);
        System.out.println("The length is"+r2.getLength()+"the width is"+r2.getWidth()+"and the area is:"+r2.getArea());
        Drawing d1=new Drawing();
        System.out.println("the shape is:"+d1.drawShapes());
        Drawing d2=new Drawing("something");
        System.out.println("another shape is:"+d2.drawShapes());


    }
}